package net.kenzan.helloworld.domain.dao;

import com.kenzan.command.api.PeopleApi;
import com.kenzan.dao.impl.EnterprisePeopleDaoImpl;
import com.kenzan.entities.DomainResultCode;
import com.kenzan.entities.Person;
import com.kenzan.helpers.HelloworldResponseHelpers;
import com.kenzan.responses.FetchPeopleResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EnterprisePeopleDaoTest {

    private static final String USER_ID_DUMMY = "some_id";

    private static final List<Person> people = Arrays.asList(
            new Person("Rob", "rval@gmail.com", "1231", LocalDate.now()),
            new Person("John", "john@gmail.com", "12345", LocalDate.now()),
            new Person("Scott", "scott@gmail.com", "3345", LocalDate.now())
    );

    private FetchPeopleResponse fetchPeopleResponse;

    @InjectMocks
    private EnterprisePeopleDaoImpl enterprisePeopleDao;

    @Mock
    private PeopleApi peopleApi;

    @Before
    public void init() {
        fetchPeopleResponse = HelloworldResponseHelpers
                .getFetchPeopleResponse(people, DomainResultCode.SUCCESS, "Success");
    }

    @Test
    public void fetchPeopleDaoTest() {
        when(peopleApi.queryPeople(USER_ID_DUMMY)).thenReturn(fetchPeopleResponse);

        Response response = enterprisePeopleDao.fetchPeople(USER_ID_DUMMY);

        assertNotNull(response.getEntity());

        FetchPeopleResponse fetchResponse = (FetchPeopleResponse) response.getEntity();
        assertEquals(fetchPeopleResponse.getCode(), fetchResponse.getCode());
        assertEquals(3, fetchResponse.getPeople().size());
    }
}
