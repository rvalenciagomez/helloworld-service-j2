/*
 * Copyright 2018, Charter Communications, All rights reserved.
 */
package com.kenzan.api.impl;


import com.google.common.base.Preconditions;
import com.google.inject.Inject;
import com.kenzan.api.V1ApiDelegate;
import com.kenzan.entities.DomainResultCode;
import com.kenzan.entities.Person;
import com.kenzan.security.context.DomainServiceException;
import com.kenzan.service.ServiceApptsDomainService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;

/**
 * @author rvalencia
 */
public class V1ApiDelegateImpl implements V1ApiDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(V1ApiDelegateImpl.class);

    private final ServiceApptsDomainService domainService;

    @Inject
    public V1ApiDelegateImpl (ServiceApptsDomainService domainService) {
        this.domainService = domainService;
    }


    @Override
    public Response getPeople (String xDomainSessionIdentity, String userId) {
        Preconditions.checkNotNull(xDomainSessionIdentity);
        Preconditions.checkNotNull(userId);
        return domainService.getPeople(xDomainSessionIdentity, userId);
    }

    @Override
    public Response addPerson (Person person) {
        Preconditions.checkNotNull(person);
        throw new DomainServiceException("Endpoint not implemented yet.", DomainResultCode.NOT_IMPLEMENTED);
    }

}
