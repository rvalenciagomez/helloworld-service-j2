package com.kenzan.api;

import com.google.inject.Inject;
import com.kenzan.entities.Person;
import com.kenzan.responses.FetchPeopleResponse;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import javax.servlet.http.HttpServlet;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path ("v1/people")

@Consumes ({"application/json"})
@Produces ({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@OpenAPIDefinition (
        info = @Info(
                title = "Helloworld v1 API",
                version = "0.0",
                description = "the v1 API"
        )
)
public class V1ApiRS extends HttpServlet {

    @Inject
    private V1ApiDelegate delegate;

    public V1ApiRS() {
    }

    @GET
    @Path ("/{userId}")
    @Consumes ({"application/json"})
    @Produces ({"application/json"})
    @Operation (
            summary = "Get people",
            description = "Get people from Api",
            tags = "helloworld",
            responses =
                    {@ApiResponse (
                            responseCode = "200",
                            description = "successful operation",
                            content= @Content(schema = @Schema(implementation = FetchPeopleResponse.class))
                    ), @ApiResponse(
                            responseCode = "400",
                            description= "Used for a 400 HTTP response - Malformed request",
                            content= @Content(schema = @Schema(implementation = FetchPeopleResponse.class))
                    ), @ApiResponse(
                            responseCode = "401",
                            description = "Used for a 401 HTTP response - Client not authenticated",
                            content= @Content(schema = @Schema(implementation = FetchPeopleResponse.class))
                    ), @ApiResponse(
                            responseCode = "403",
                            description = "The logged-in user is not permitted to perform the current operation.",
                            content= @Content(schema = @Schema(implementation = FetchPeopleResponse.class))
                    ), @ApiResponse(
                            responseCode = "404",
                            description = "Used for a 404 HTTP response - Information or resource not found",
                            content= @Content(schema = @Schema(implementation = FetchPeopleResponse.class))
                    ), @ApiResponse(
                            responseCode =  "500",
                            description = "Used for a 500 HTTP response - Internal server error",
                            content= @Content(schema = @Schema(implementation = FetchPeopleResponse.class))
                    ), @ApiResponse(
                            responseCode = "200",
                            description = "Used for a 500 HTTP response - Internal server error",
                            content= @Content(schema = @Schema(implementation = FetchPeopleResponse.class))
                    )
                    }
    )
    public Response delegateGetPeople(@Parameter ( description = "Session identity information. This contains the SessionIdentityInfo object that has been serialized to JSON and then base64 encoded. Passing of this header is not required for non-domain service client implementations, but should be provided for domain service to domain service communication. ")  @HeaderParam ("X-DOMAIN-SESSION-IDENTITY") String xDomainSessionIdentity, @PathParam ("userId") String userId) {
        return delegate.getPeople(xDomainSessionIdentity, userId);
    }

    @POST
    @Path ("/")
    @Consumes ({"application/json"})
    @Produces ({"application/json"})
    @Operation (
        summary = "Add new person",
        description = "Add new person from Api",
        tags = "helloworld",
        responses =
            {@ApiResponse (
                    responseCode = "200",
                    description = "successful operation",
                    content= @Content(schema = @Schema(implementation = FetchPeopleResponse.class))
            ), @ApiResponse(
                    responseCode = "400",
                    description= "Used for a 400 HTTP response - Malformed request",
                    content= @Content(schema = @Schema(implementation = FetchPeopleResponse.class))
            ), @ApiResponse(
                    responseCode = "401",
                    description = "Used for a 401 HTTP response - Client not authenticated",
                    content= @Content(schema = @Schema(implementation = FetchPeopleResponse.class))
            ), @ApiResponse(
                    responseCode = "403",
                    description = "The logged-in user is not permitted to perform the current operation.",
                    content= @Content(schema = @Schema(implementation = FetchPeopleResponse.class))
            ), @ApiResponse(
                    responseCode = "404",
                    description = "Used for a 404 HTTP response - Information or resource not found",
                    content= @Content(schema = @Schema(implementation = FetchPeopleResponse.class))
            ), @ApiResponse(
                    responseCode =  "500",
                    description = "Used for a 500 HTTP response - Internal server error",
                    content= @Content(schema = @Schema(implementation = FetchPeopleResponse.class))
            ), @ApiResponse(
                    responseCode = "200",
                    description = "Used for a 500 HTTP response - Internal server error",
                    content= @Content(schema = @Schema(implementation = FetchPeopleResponse.class))
            )
            }
    )
    public Response delegateAddPerson(@Parameter(description= "Request to create person", required = true) Person person) {
        return delegate.addPerson(person);
    }


}
