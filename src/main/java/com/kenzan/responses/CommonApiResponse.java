package com.kenzan.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.Objects;


public class CommonApiResponse {
    protected final String txnId;
    protected final OffsetDateTime timestamp;
    protected final String message;

    public CommonApiResponse(String txnId, OffsetDateTime timestamp, String message) {
        this.txnId = txnId;
        this.timestamp = timestamp;
        this.message = message;
    }

    @JsonCreator
    public static CommonApiResponse create(@JsonProperty ("txnId") String txnId, @JsonProperty("timestamp") OffsetDateTime timestamp, @JsonProperty("message") String message) {
        return (new Builder()).withTxnId(txnId).withTimestamp(timestamp).withMessage(message).build();
    }

    @JsonProperty("txnId")
    @Schema (
            required = true,
            description = "Transaction ID"
    )
    @NotNull
    public String getTxnId() {
        return this.txnId;
    }

    @JsonProperty("timestamp")
    @Schema(
            required = true,
            description = "Timestamp of response"
    )
    @NotNull
    public OffsetDateTime getTimestamp() {
        return this.timestamp;
    }

    @JsonProperty("message")
    @Schema( description = "Human readable description of response")
    public String getMessage() {
        return this.message;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o != null && this.getClass() == o.getClass()) {
            CommonApiResponse commonApiResponse = (CommonApiResponse)o;
            return Objects.equals(this.txnId, commonApiResponse.txnId) && Objects.equals(this.timestamp, commonApiResponse.timestamp) && Objects.equals(this.message, commonApiResponse.message);
        } else {
            return false;
        }
    }

    public int hashCode() {
        return Objects.hash(new Object[]{this.txnId, this.timestamp, this.message});
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class CommonApiResponse {\n");
        sb.append("    txnId: ").append(this.toIndentedString(this.txnId)).append("\n");
        sb.append("    timestamp: ").append(this.toIndentedString(this.timestamp)).append("\n");
        sb.append("    message: ").append(this.toIndentedString(this.message)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    private String toIndentedString(Object o) {
        return o == null ? "null" : o.toString().replace("\n", "\n    ");
    }

    public String toJson() {
        return (new Gson()).toJson(this);
    }

    public CommonApiResponse fromJson(String json) {
        return (CommonApiResponse)(new Gson()).fromJson(json, CommonApiResponse.class);
    }

    public static class Builder {
        private String txnId;
        private OffsetDateTime timestamp;
        private String message;

        public Builder() {
        }

        public Builder withTxnId(String txnId) {
            this.txnId = txnId;
            return this;
        }

        public Builder withTimestamp(OffsetDateTime timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public CommonApiResponse build() {
            CommonApiResponse temp = new CommonApiResponse(this.txnId, this.timestamp, this.message);
            return temp;
        }

        public Builder fromExisting(CommonApiResponse existing) {
            Builder builder = new Builder();
            builder.withTxnId(existing.getTxnId());
            builder.withTimestamp(existing.getTimestamp());
            builder.withMessage(existing.getMessage());
            return builder;
        }
    }
}
