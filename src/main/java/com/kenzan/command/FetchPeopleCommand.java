/*
 * Copyright 2018, Charter Communications,  All rights reserved.
 */
package com.kenzan.command;

import com.kenzan.command.api.PeopleApi;
import com.kenzan.responses.FetchPeopleResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class FetchPeopleCommand extends DomainCommand<FetchPeopleResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(FetchPeopleCommand.class);

    private final PeopleApi peopleApi;

    private static String userId;

    public FetchPeopleCommand (String userId, PeopleApi peopleApi) {
        super(FetchPeopleCommand.class.getSimpleName());
        this.userId = userId;
        this.peopleApi = peopleApi;
    }

    @Override
    protected FetchPeopleResponse run () throws Exception {

        LOGGER.info("userId: {}", userId);

        return peopleApi.queryPeople(userId);
    }

}

