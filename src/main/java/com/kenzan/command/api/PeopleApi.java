package com.kenzan.command.api;


import com.kenzan.responses.FetchPeopleResponse;

public interface PeopleApi {

    public FetchPeopleResponse queryPeople (String userId);
}
