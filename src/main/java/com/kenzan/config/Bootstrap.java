/*
 * Copyright 2018, Charter Communications, All rights reserved.
 */
package com.kenzan.config;

import com.google.inject.Binder;
import com.kenzan.config.bootstrap.ServerBootstrap;
import com.kenzan.config.module.HelloworldModule;
import com.kenzan.config.module.RestModule;


/**
 * starts the service in PROD mode
 */
public class Bootstrap extends ServerBootstrap {

    @Override
    protected void configureBinder(Binder binder) {
        binder.install(new ObjectMapperModule());
        binder.install(new RestModule());
        binder.install(new HelloworldModule());
    }

}