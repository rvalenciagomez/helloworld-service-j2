package com.kenzan.config.module;

import javax.inject.Singleton;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.kenzan.HelloService;
import com.kenzan.HelloServiceImpl;
import com.kenzan.ThrowableMapper;
import com.kenzan.HelloResource;
import com.kenzan.api.V1ApiDelegate;
import com.kenzan.api.impl.V1ApiDelegateImpl;
import com.kenzan.command.api.PeopleApi;
import com.kenzan.command.api.PeopleApiImpl;
import com.kenzan.dao.EnterprisePeopleDao;
import com.kenzan.dao.impl.EnterprisePeopleDaoImpl;
import com.kenzan.service.ServiceApptsDomainService;
import com.kenzan.service.impl.ServiceApptsDomainServiceImpl;

public class HelloworldModule extends AbstractModule {

  @Override
  protected void configure() {

    bind(HelloResource.class);
    bind(ThrowableMapper.class);

    bind(V1ApiDelegate.class).to(V1ApiDelegateImpl.class).in(Scopes.SINGLETON);
    bind(PeopleApi.class).to(PeopleApiImpl.class).in(Scopes.SINGLETON);
    bind(EnterprisePeopleDao.class).to(EnterprisePeopleDaoImpl.class).in(Scopes.SINGLETON);
    bind(ServiceApptsDomainService.class).to(ServiceApptsDomainServiceImpl.class).in(Scopes.SINGLETON);

  }

  @Provides
  @Singleton
  public HelloService helloService() {
    return new HelloServiceImpl();
  }

}
