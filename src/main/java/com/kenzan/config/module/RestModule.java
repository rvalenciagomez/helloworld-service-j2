package com.kenzan.config.module;

import com.google.inject.Scopes;
import com.kenzan.security.context.DomainServiceExceptionMapper;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public class RestModule extends JerseyServletModule {

   protected static final Logger LOGGER = LoggerFactory.getLogger(RestModule.class);

   private static final String PACKAGES =   "io.swagger.v3.jaxrs2.integration.resources " +
                                            "com.kenzan.api " +
                                            "com.kenzan.config " +
                                            "net.spectrum.portal.healthcheck";
//   public RestModule () {
//      register(HelloResource.class);
//      register(V1ApiRS.class);
//      register(ThrowableMapper.class);
//      register(OpenApiResource.class, AcceptHeaderOpenApiResource.class);
//   }

   @Override
   protected void configureServlets () {
      HashMap<String, String> initParams = new HashMap<>();
//        initParams.put(ResourceConfig.PROPERTY_RESOURCE_FILTER_FACTORIES, FACTORIES);
     initParams.put("com.sun.jersey.config.property.packages", PACKAGES);
     serve("/*").with(GuiceContainer.class, initParams);
     LOGGER.info("Registering injectables..");
     bind(DomainServiceExceptionMapper.class).in(Scopes.SINGLETON);
   }
}
