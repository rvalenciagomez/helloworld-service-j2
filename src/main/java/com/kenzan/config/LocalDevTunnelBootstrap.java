/*
 * Copyright 2018, Charter Communications,  All rights reserved.
 */
package com.kenzan.config;


import com.google.inject.Binder;
import com.kenzan.config.bootstrap.ServerBootstrap;
import com.kenzan.config.module.HelloworldModule;
import com.kenzan.config.module.RestModule;
import com.squarespace.jersey2.guice.JerseyGuiceModule;


/**
 * starts the service in LOCAL DEV TUNNEL mode. the service is started locally and expects communication with
 * other services to be facilitated via ssh tunnel
 *
 */
public class LocalDevTunnelBootstrap extends ServerBootstrap {

    @Override
    protected void configureBinder(Binder binder) {
        binder.install(new ObjectMapperModule());
        binder.install(new RestModule());
        binder.install(new HelloworldModule());
    }

}
