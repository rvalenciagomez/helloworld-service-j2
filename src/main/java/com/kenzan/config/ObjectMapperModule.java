package com.kenzan.config;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import java.text.SimpleDateFormat;

public class ObjectMapperModule extends AbstractModule {
    public static final String RESPONSE_OBJECT_MAPPER = "ResponseObjectMapper";

    public ObjectMapperModule () {
    }

    protected void configure() {
    }

    @Provides
    @Singleton
    @Named ("ResponseObjectMapper")
    public ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.disable(new MapperFeature[]{MapperFeature.USE_GETTERS_AS_SETTERS});
        objectMapper.setSerializationInclusion(Include.NON_NULL);
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper;
    }

    @Provides
    @Singleton
    @Inject
    public JacksonJaxbJsonProvider getJacksonJaxbJsonProvider(@Named ("ResponseObjectMapper") ObjectMapper objectMapper) {
        JacksonJaxbJsonProvider jacksonJaxbJsonProvider = new JacksonJaxbJsonProvider();
        jacksonJaxbJsonProvider.setMapper(objectMapper);
        return jacksonJaxbJsonProvider;
    }
}
