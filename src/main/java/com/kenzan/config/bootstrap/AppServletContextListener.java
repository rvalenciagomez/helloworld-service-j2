package com.kenzan.config.bootstrap;

import com.google.common.base.Throwables;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Stage;
import com.google.inject.servlet.GuiceServletContextListener;
import com.google.inject.servlet.ServletModule;
import com.kenzan.config.ObjectMapperModule;
import com.kenzan.config.module.HelloworldModule;
import com.netflix.config.ConfigurationManager;
import com.squarespace.jersey2.guice.JerseyGuiceModule;
import com.squarespace.jersey2.guice.JerseyGuiceUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class AppServletContextListener extends GuiceServletContextListener {

  protected static final Logger LOGGER = LoggerFactory.getLogger(AppServletContextListener.class);

  @Override
  protected Injector getInjector() {
    this.loadPropertiesFromResources();

//    Injector injector = Guice.createInjector(
//      Stage.PRODUCTION,
//      new JerseyGuiceModule("__HK2_Generated_0"),
//      new ServletModule(),
//      new HelloworldModule(),
//      new ObjectMapperModule()
//    );
//
//    JerseyGuiceUtils.install(injector);
//
//    return injector;
    ServerBootstrap serverBootstrap = this.instantiateBootstrapClass();
    return serverBootstrap.bootstrap();

  }

  private ServerBootstrap instantiateBootstrapClass() {

    String bootstrapClassName = ConfigurationManager.getConfigInstance().getString("com.kenzan.config.bootstrap.class");

    try {
      return (ServerBootstrap)Class.forName(bootstrapClassName).newInstance();
    } catch (Exception e) {
      LOGGER.error(String.format("Failed to instantiate server bootstrap class %s", new Object[]{bootstrapClassName}), e);
      throw Throwables.propagate(e);
    }
  }

  private void loadPropertiesFromResources() {
    String appId = ConfigurationManager.getDeploymentContext().getApplicationId();
    if(appId != null) {
      try {
        LOGGER.info(String.format("Loading application properties with app id: %s and environment: %s", new Object[]{appId, ConfigurationManager.getDeploymentContext().getDeploymentEnvironment()}));
        ConfigurationManager.loadCascadedPropertiesFromResources(appId);
      } catch (IOException e) {
        LOGGER.error(String.format("Failed to load properties for application id: %s and environment: %s. This is ok, if you do not have application level properties.", new Object[]{appId, ConfigurationManager.getDeploymentContext().getDeploymentEnvironment()}), e);
      }
    } else {
      LOGGER.warn("Application identifier not defined, skipping application level properties loading. You must set a property \'archaius.deployment.applicationId\' to be able to load application level properties.");
    }

  }

}
