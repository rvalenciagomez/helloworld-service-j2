/*
 * Copyright 2018, Charter Communications,  All rights reserved.
 */
package com.kenzan.dao.impl;


import com.google.inject.Inject;

import com.kenzan.command.FetchPeopleCommand;
import com.kenzan.command.api.PeopleApi;
import com.kenzan.dao.EnterprisePeopleDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;


public class EnterprisePeopleDaoImpl implements EnterprisePeopleDao {

    Logger LOGGER = LoggerFactory.getLogger(EnterprisePeopleDaoImpl.class);

    private final PeopleApi peopleApi;

    @Inject
    public EnterprisePeopleDaoImpl (PeopleApi peopleApi) {
        this.peopleApi = peopleApi;
    }

    @Override
    public Response fetchPeople (String userId) {
        return Response.ok(
                new FetchPeopleCommand(userId, peopleApi)
                        .observe()
                        .toBlocking()
                        .single()
            ).build();
    }
}



