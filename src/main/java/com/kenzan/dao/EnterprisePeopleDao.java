/*
 * Copyright 2018, Charter Communications,  All rights reserved.
 */
package com.kenzan.dao;


import javax.ws.rs.core.Response;


public interface EnterprisePeopleDao {

    Response fetchPeople (String userId);
}
