package com.kenzan.entities;

import java.time.LocalDate;

public class Person {

    private String name;
    private String email;
    private String phone;
    private LocalDate creationDate;

    public Person () {
    }

    public Person (String name, String email, String phone, LocalDate creationDate) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.creationDate = creationDate;
    }

    public String getName () {
        return name;
    }

    public String getEmail () {
        return email;
    }

    public String getPhone () {
        return phone;
    }

    public LocalDate getCreationDate () {
        return creationDate;
    }

    @Override
    public String toString () {
        return "Person{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}
