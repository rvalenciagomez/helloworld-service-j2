/*
 * Copyright 2018, Charter Communications,  All rights reserved.
 */
package com.kenzan.service.impl;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.kenzan.dao.EnterprisePeopleDao;
import com.kenzan.entities.Person;
import com.kenzan.entities.SessionIdentityInfo;
import com.kenzan.helpers.Decoder;
import com.kenzan.responses.FetchPeopleResponse;
import com.kenzan.service.ServiceApptsDomainService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.util.Optional;

import static com.kenzan.helpers.HelloworldResponseHelpers.getResponseWithCommonApiResponse;


/**
 * business logic layer for helloworld
 */
public class ServiceApptsDomainServiceImpl implements ServiceApptsDomainService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceApptsDomainServiceImpl.class);

    private final EnterprisePeopleDao enterprisePeopleDao;
    private final Decoder decoder;

    /**
     * @param enterprisePeopleDao
     */
    @Inject
    public ServiceApptsDomainServiceImpl(
            @Named ("ResponseObjectMapper") ObjectMapper objectMapper,
            EnterprisePeopleDao enterprisePeopleDao) {

        this.enterprisePeopleDao = enterprisePeopleDao;
        this.decoder = new Decoder();
    }


    @Override
    public Response getPeople(String xDomainSessionIdentity, String userId) {

        SessionIdentityInfo sessionIdentityInfo = decoder.decode(xDomainSessionIdentity, SessionIdentityInfo.class);
        LOGGER.info("getPeople - sessionIdentityInfo: {}", sessionIdentityInfo.toJson());

        if(sessionIdentityInfo.getAccountNumber() == null || sessionIdentityInfo.getGuid() == null) {
            LOGGER.error("Error due to accountNumber or Guid empty in sessionIdentityInfo");
            return getResponseWithCommonApiResponse(Optional.empty(), Response.Status.INTERNAL_SERVER_ERROR);
        }

        Response serviceResponse = enterprisePeopleDao.fetchPeople(userId);

        if (serviceResponse == null) {
            LOGGER.error("Failure in enterpriseWorkOrderImpl.getRescheduleAvailability response");
            return getResponseWithCommonApiResponse(Optional.empty(), Response.Status.INTERNAL_SERVER_ERROR);
        }

        if(serviceResponse.getEntity() instanceof FetchPeopleResponse) {
            FetchPeopleResponse fetchPeopleResponse = (FetchPeopleResponse) serviceResponse.getEntity();

            return fetchPeopleResponse.getPeople().isEmpty()
                    ? getResponseWithCommonApiResponse(Optional.empty(), Response.Status.NOT_FOUND)
                    : serviceResponse;
        }
        return serviceResponse;
    }

}
