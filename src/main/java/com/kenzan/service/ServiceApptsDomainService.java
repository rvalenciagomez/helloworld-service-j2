/*
 * Copyright 2018, Charter Communications,  All rights reserved.
 */
package com.kenzan.service;


import com.kenzan.entities.Person;
import javax.ws.rs.core.Response;


/**
 *
 * @author rvalencia
 */
public interface ServiceApptsDomainService {

    /**
     * Authenticates a given identity
     *
     * @param xDomainSessionIdentity
     * @return Response
     */
    Response getPeople (String xDomainSessionIdentity, String userId);

}
