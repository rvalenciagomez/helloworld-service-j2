#Helloworld Domain Service

The helloworld-kenzan-service is a [maven](https://maven.apache.org/ "maven") project using [jersey](https://jersey.github.io/ "jersey") to expose its rest endpoints. 

We use google [Guice](https://github.com/google/guice/wiki/Injections, "Guice") module injection to boostrap the application. Property configuration is achieved using [Archaius](https://github.com/Netflix/archaius "Archaius")

The helloworld kenzan web service provides to retrieve people from a mocked WireMock service, so far. This typically includes:

* Helloworld Method Code
* Helloworld Method Sequence

## Glossary

* 

## Prerequisites

### Java
This application assumes the Java compilation and runtime version is 1.8 (Java 8). Java install instructions and downloadables can be found [here](http://www.oracle.com/technetwork/java/javase/downloads/index.html "here")

### Maven
This project utilizes Maven as a build tool. Please refer to the [maven page](https://maven.apache.org/install.html "maven page") to install maven on your local machine.

Once these tools are installed run the following maven command to compile and package the service: 

`mvn clean package`

## Running Locally

### local profile

The local profile uses stub data. To run the service with this profile, execute:

`mvn -P local tomcat7:run`

### local-tunnel profile

The local-tunnel profile uses real-time data. A few tunnels need to be opened in order for it to work. To run the service with this profile, execute:

`mvn -P local-tunnel tomcat7:run`

### int-test profile

The int-test profile uses stubbed data with WireMock. To get the response back in order for it to work. To run the service with this profile, execute:

`mvn -P local-tunnel tomcat7:run`

To start WireMock, go to other terminal, same root project dir and `mvn docker:build` and `mvn docker:run`. As documented in the docker/server/README.md

## Example requests

Example Postman requests can be found in the folder /docs

### cURL Example

Matching userId:
`curl -X GET \
"http://localhost:8889/helloworldkenzanservice/v1/people/111" \
-H "X-DOMAIN-SESSION-IDENTITY: eyJhY2NvdW50TnVtYmVyIjoiMTIzNDU2Nzg5IiwiZ3VpZCI6IjEyMzQ1Njc4OSIsInVzZXJuYW1lIjoiVEVTVCIsImNsaWVudElwQWRkcmVzcyI6IjE5Mi4xNjguMS4xIiwic2Vzc2lvbklkIjoiNkFTREZBLU1WOU4wLTQ1RjlDMFEifQ=="
`
Not matching userId, with 404 code:
`curl -X GET \
"http://localhost:8889/helloworldkenzanservice/v1/people/112" \
-H "X-DOMAIN-SESSION-IDENTITY: eyJhY2NvdW50TnVtYmVyIjoiIiwiZ3VpZCI6IjEyMzQ1Njc4OSIsInVzZXJuYW1lIjoiVEVTVCIsImNsaWVudElwQWRkcmVzcyI6IjE5Mi4xNjguMS4xIiwic2Vzc2lvbklkIjoiNkFTREZBLU1WOU4wLTQ1RjlDMFEifQ=="
`

## Api Documentation

`http://localhost:8889/helloworldkenzanservice/openapi.json`
* When running in local, or change host depending on the environment

## Publish pacts

`mvn pact:publish`
* Pacts need to be generated in /target/pacts beforehand by running tests 

## Running integration tests

In order to kick off the integration tests, use the following:

`mvn verify -Pint-test`


## Links


## Versioning

[SemVer](http://semver.org/) for versioning 
